<?php

declare(strict_types=1);

namespace Drupal\Tests\songbook\Kernel;

use Drupal\Core\Language\Language;
use Drupal\filter\FilterPluginCollection;
use Drupal\filter\Plugin\FilterInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test the songbook filter.
 */
class FilterSongbookTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'filter',
    'songbook',
  ];

  /**
   * The songbook filter.
   */
  protected FilterInterface $songbookFilter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $filter_plugin_manager */
    $filter_plugin_manager = $this->container->get('plugin.manager.filter');
    $this->songbookFilter = (new FilterPluginCollection($filter_plugin_manager, []))
      ->get('filter_songbook');
  }

  /**
   * Test the songbook filter.
   *
   * @dataProvider provideSongbookFilterTestData
   */
  public function testSongbookFilter(array $source_notation, string $target_notation, bool $hide_chords, string $format, string $ignore_metadata, string $content, string $expected_result) {
    $this->songbookFilter->setConfiguration([
      'settings' => [
        'source_notation' => $source_notation,
        'target_notation' => $target_notation,
        'hide_chords' => $hide_chords,
        'format' => $format,
        'ignore_metadata' => $ignore_metadata,
      ],
    ]);

    $processed = (string) $this->songbookFilter->process($content, Language::LANGCODE_NOT_SPECIFIED);
    $processed_dom = new \DOMDocument();
    $processed_dom->formatOutput = TRUE;
    $processed_dom->loadHTML($processed, LIBXML_NOBLANKS);
    $expected_dom = new \DOMDocument();
    $expected_dom->formatOutput = TRUE;
    $expected_dom->loadHTML($expected_result, LIBXML_NOBLANKS);
    $this->assertEquals($processed_dom->saveHTML(), $expected_dom->saveHTML());
  }

  /**
   * Test data provider.
   */
  public function provideSongbookFilterTestData() {
    yield 'Basic HTML chord' => [
      'source_notation' => ['utf'],
      'target_notation' => 'utf',
      'hide_chords' => FALSE,
      'format' => 'html',
      'ignore_metadata' => '',
      'content' => '[C]Hello [G]world!',
      'expected_result' => '<div class="chordpro-line"><span class="chordpro-block"><span class="chordpro-chord" data-chord="C">C</span><span class="chordpro-text">Hello&nbsp;</span></span><span class="chordpro-block"><span class="chordpro-chord" data-chord="G">G</span><span class="chordpro-text">world!</span></span></div>',
    ];
    yield 'Basic JSON chord' => [
      'source_notation' => ['utf'],
      'target_notation' => 'utf',
      'hide_chords' => FALSE,
      'format' => 'json',
      'ignore_metadata' => '',
      'content' => '[C]Hello [G]world!',
      'expected_result' => <<<END
<p>[
    {
        "type": "line",
        "blocks": [
            {
                "text": "Hello",
                "chord": "C",
                "originalChord": "C"
            },
            {
                "text": "world!",
                "chord": "G",
                "originalChord": "G"
            }
        ]
    }
]</p>
END
    ];
    yield 'Basic Monospace chord' => [
      'source_notation' => ['utf'],
      'target_notation' => 'utf',
      'hide_chords' => FALSE,
      'format' => 'monospace',
      'ignore_metadata' => '',
      'content' => '[C]Hello [G]world!',
      'expected_result' => "<pre>C     G     \nHello world!\n</pre>",
    ];
    yield 'Testing notations' => [
      'source_notation' => ['utf', 'german'],
      'target_notation' => 'french',
      'hide_chords' => FALSE,
      'format' => 'monospace',
      'ignore_metadata' => '',
      'content' => '[Cis]Hello [H]world!',
      'expected_result' => "<pre>Do♯   Si    \nHello world!\n</pre>",
    ];
    yield 'Testing hide chords' => [
      'source_notation' => ['utf'],
      'target_notation' => 'utf',
      'hide_chords' => TRUE,
      'format' => 'monospace',
      'ignore_metadata' => '',
      'content' => '[Cis]Hello [H]world!',
      'expected_result' => "<pre>Hello world!\n</pre>",
    ];
    yield 'Testing ignoring metadata' => [
      'source_notation' => ['utf'],
      'target_notation' => 'utf',
      'hide_chords' => TRUE,
      'format' => 'monospace',
      'ignore_metadata' => 'title',
      'content' => "{t:Title}\n{st:Subtitle}\n[C]Hello [D]world!",
      'expected_result' => "<pre>Subtitle\nHello world!\n</pre>",
    ];
  }

}
