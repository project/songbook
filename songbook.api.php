<?php

/**
 * @file
 * Hooks related to the Songbook module.
 */

/**
 * Alter the list of chord notations available to the Songbook module.
 *
 * @param array $notations
 *   An array of available chord notations, containing:
 *   - name: The name of the notation to display on the form.
 *   - class: The class that implements the notation.
 */
function mymodule_songbook_notations_alter(&$notations) {
  $notations['my_notation'] = [
    'name' => t('Notation name to display on the form'),
    'class' => 'Drupal\mymodule\MyCustomChordNotation',
  ];
}

/**
 * Alter the list of output formats available to the Songbook module.
 *
 * @param array $formats
 *   An array of available output formats, containing:
 *   - name: The name of the format to display on the form.
 *   - class: The class that implements the format.
 *   - library: The optional Drupal library to include.
 *   - prefix: The optional prefix to add to the output.
 *   - suffix: The optional suffix to add to the output.
 */
function mymodule_songbook_formats_alter(&$formats) {
  $formats['my_formatter'] = [
    'name' => t('Formatter name to display on the form'),
    'class' => 'Drupal\mymodule\MyCustomFormatter',
    'library' => 'mymodule/myformatter',
    'prefix' => '<pre>',
    'suffix' => '</pre>',
  ];
}
