# Songbook 🎸

The Songbook module integrates the <https://github.com/intelektron/chordpro-php>
library into Drupal.

It provides a content filter to parse [ChordPro 6.x](https://www.chordpro.org/)
format.

Installation:

```bash
composer require drupal/songbook
```

To start using the module, create a new text format and enable the
`Parse as a ChordPro song with chords` filter.

## Defining custom formatters and notations

If you have your own classes extending `ChordNotationInterface` or
`FormatterInterface`, please see `songbook.api.php` file for alter hook
usage instructions.
