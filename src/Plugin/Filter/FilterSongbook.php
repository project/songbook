<?php

declare(strict_types=1);

namespace Drupal\songbook\Plugin\Filter;

use ChordPro\Formatter\FormatterInterface;
use ChordPro\Formatter\HtmlFormatter;
use ChordPro\Formatter\JSONFormatter;
use ChordPro\Formatter\MonospaceFormatter;
use ChordPro\Notation\ChordNotationInterface;
use ChordPro\Notation\FrenchChordNotation;
use ChordPro\Notation\GermanChordNotation;
use ChordPro\Notation\UtfChordNotation;
use ChordPro\Parser;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Psr\Container\ContainerInterface;

/**
 * Filter that converts text in ChordPro format into a song with chords.
 *
 * @Filter(
 *   id = "filter_songbook",
 *   title = @Translation("Parse as a ChordPro song with chords"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   settings = {
 *     "source_notation" = {"utf"},
 *     "target_notation" = "utf",
 *     "hide_chords" = false,
 *     "format" = "html",
 *     "ignore_metadata" = "title,subtitle"
 *   },
 * )
 */
final class FilterSongbook extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new SongbookBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   A cache backend interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly CacheBackendInterface $cache,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $notation_options = [];
    foreach ($this->getAvailableNotations() as $notationId => $notation) {
      $notation_options[$notationId] = $notation['name'];
    }
    $form['source_notation'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Source chord notation'),
      '#default_value' => $this->settings['source_notation'] ?? ['utf'],
      '#description' => $this->t('The source notation of the chords to parse. You can select more than one if the chord notations are mixed up.'),
      '#options' => $notation_options,
    ];
    $form['target_notation'] = [
      '#type' => 'select',
      '#title' => $this->t('Target chord notation'),
      '#default_value' => $this->settings['target_notation'] ?? 'utf',
      '#description' => $this->t('The target notation of the chords to be displayed to the user.'),
      '#options' => $notation_options,
    ];
    $format_options = [];
    foreach ($this->getAvailableFormats() as $format_id => $format) {
      $format_options[$format_id] = $format['name'];
    }
    $form['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Output format'),
      '#default_value' => $this->settings['format'] ?? 'html',
      '#description' => $this->t('The format to print the song.'),
      '#options' => $format_options,
    ];
    $form['hide_chords'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide chords'),
      '#default_value' => $this->settings['hide_chords'] ?? FALSE,
      '#description' => $this->t('Show lyrics only, without chords.'),
    ];
    $form['ignore_metadata'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ignore metadata'),
      '#default_value' => $this->settings['ignore_metadata'] ?? 'title,subtitle',
      '#description' => $this->t('Type the names of metadata excluded from display, separated by commas. For example: <code>title,subtitle</code>. Please use the full names, not the abbreviations (e.g. <code>comment</code> instead of <code>c</code>).'),
    ];
    return $form;
  }

  /**
   * Get available chord notations.
   *
   * @return array
   *   An array of available chord notations.
   */
  private function getAvailableNotations(): array {
    if ($this->cache->get('songbook_notations')) {
      return $this->cache->get('songbook_notations')->data;
    }

    $available_notations = [
      'utf' => [
        'name' => $this->t('Chords with ♭ and ♯ symbols'),
        'class' => UtfChordNotation::class,
      ],
      'german' => [
        'name' => $this->t('German (e.g. H instead of B)'),
        'class' => GermanChordNotation::class,
      ],
      'french' => [
        'name' => $this->t('French (e.g. Do instead of C)'),
        'class' => FrenchChordNotation::class,
      ],
    ];
    $this->moduleHandler->alter('songbook_notations', $available_notations);
    $this->cache->set('songbook_notations', $available_notations);
    return $available_notations;
  }

  /**
   * Get available output formats.
   *
   * @return array
   *   An array of available output formats.
   */
  private function getAvailableFormats(): array {
    if ($this->cache->get('songbook_formats')) {
      return $this->cache->get('songbook_formats')->data;
    }
    $available_formats = [
      'html' => [
        'name' => $this->t('HTML'),
        'class' => HtmlFormatter::class,
        'library' => 'songbook/html',
      ],
      'json' => [
        'name' => $this->t('JSON'),
        'class' => JSONFormatter::class,
      ],
      'monospace' => [
        'name' => $this->t('Plain text / Monospace'),
        'class' => MonospaceFormatter::class,
        'prefix' => '<pre>',
        'suffix' => '</pre>',
      ],
    ];
    $this->moduleHandler->alter('songbook_formats', $available_formats);
    $this->cache->set('songbook_formats', $available_formats);
    return $available_formats;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $parser = new Parser();

    // Source notations.
    $available_notations = $this->getAvailableNotations();
    $source_notations = [];
    foreach ($this->settings['source_notation'] ?? [] as $notation) {
      if ($notation && isset($available_notations[$notation])) {
        $newNotation = new $available_notations[$notation]['class']();
        if ($newNotation instanceof ChordNotationInterface) {
          $source_notations[] = $newNotation;
        }
      }
    }
    $song = $parser->parse($text, $source_notations);

    // Target notation.
    $format_options = [];
    if (isset($this->settings['target_notation']) && $this->settings['target_notation'] && isset($available_notations[$this->settings['target_notation']])) {
      $format_options['notation'] = new $available_notations[$this->settings['target_notation']]['class']();
    }

    // Hide chords.
    if ($this->settings['hide_chords'] ?? FALSE) {
      $format_options['no_chords'] = TRUE;
    }

    // Ignore metadata.
    if ($this->settings['ignore_metadata'] ?? FALSE) {
      $format_options['ignore_metadata'] = array_map('trim', explode(',', $this->settings['ignore_metadata']));
    }

    // Choose format.
    $available_formats = $this->getAvailableFormats();
    if (isset($this->settings['format']) && $this->settings['format'] && isset($available_formats[$this->settings['format']])) {
      $current_format = $available_formats[$this->settings['format']];
      $formatter = new $current_format['class']();
      assert($formatter instanceof FormatterInterface);
      $formatted_song = $formatter->format($song, $format_options);
      if (isset($current_format['prefix'])) {
        $formatted_song = $current_format['prefix'] . $formatted_song;
      }
      if (isset($current_format['suffix'])) {
        $formatted_song .= $current_format['suffix'];
      }
      $output = new FilterProcessResult($formatted_song);
      if (isset($current_format['library'])) {
        $output->addAttachments([
          'library' => [
            $current_format['library'],
          ],
        ]);
      }
      return $output;
    }

    // If everything fails, return the original text.
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('
      <p>You can use <a href="https://www.chordpro.org/chordpro/chordpro-introduction/">ChordPro 6.x</a> markup, with chords and metadata.</p>
      <ul>
        <li>Put the chords in square brackets: <code>This is the [Am]Sample line [C/E]Of the song</code></li>
        <li>Organize the song into sections: <code>{start_of_chorus} ... {end_of_chorus}</code></li>
        <li>Use ChordPro visible comments: <code>{c: Sing it slowly}</code></li>
        <li>Use ChordPro code comments: <code># This fragment may be wrong</code></li>
        <li>Use ChordPro metadata: <code>{key:C} {capo:2}</code></li>
      </ul>')->render();
  }

}
